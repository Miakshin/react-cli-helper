const fs = require("fs");
const fsPromises = fs.promises;

const createDir = async dirPath => fsPromises.mkdir(dirPath, { recursive: true });

module.exports = createDir;