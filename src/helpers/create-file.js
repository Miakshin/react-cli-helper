const fs = require("fs");
const fsPromises = fs.promises;

const logger = require("./logger").logger;
const SUCCESS = require("./logger").SUCCESS;

const createFile = async (filePath, fileData) => {
  const successText = `${filePath} was created`;
  return fsPromises
    .writeFile(filePath, fileData, { flag: "as+" })
    .then(() => logger(SUCCESS, successText));
};

module.exports = createFile;
