const path = require("path");

const getFileNameFromPath = pathString => {
  const fileName = path.basename(pathString);
  return fileName.split('.')[0];
};

module.exports = getFileNameFromPath;
