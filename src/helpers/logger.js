const chalk = require("chalk");
const SUCCESS = "success";
const ERROR = "error";

const availableTypes = [SUCCESS, ERROR];
const logger = (type, text) => {
  if (!availableTypes.includes(type)) {
    throw `type should be one of ${availableTypes}`;
  }
  type === SUCCESS
    ? logSuccess(text)
    : type === ERROR
    ? logError(text)
    : null;
};

const logError = text => {
  console.log(chalk.red(text));
};

const logSuccess = text => {
  console.log(chalk.green(text));
};

module.exports = {
  logger,
  SUCCESS,
  ERROR,
};
