const GENERATE = "generate";
const HELP = "help";
const COMPONENT = "component";
const ROUTER = "router";
const HOC = "hoc";

const commands = [GENERATE, HELP];

const subCommandsMaps = {
  generate: [COMPONENT, ROUTER, HOC],
  help: null
};

COMPONENT_TYPE = 'COMPONENT_TYPE';
ROUTER_TYPE = 'ROUTER_TYPE';
HOC_TYPE = 'HOC_TYPE';
STYLE_TYPE = 'STYLE_TYPE';
INTERFACE_TYPE = 'INTERFACE_TYPE';

const filesExtensionsMap = {
  [COMPONENT_TYPE]: '.component.tsx',
  [ROUTER_TYPE]: 'router.tsx',
  [HOC_TYPE]: '.hoc.tsx',
  [STYLE_TYPE]: '.style.css',
  [INTERFACE_TYPE]: '.types.ts',
};

module.exports = {
  commands,
  subCommandsMaps,
  filesExtensionsMap,
  COMPONENT_TYPE,
  ROUTER_TYPE,
  HOC_TYPE,
  STYLE_TYPE,
  INTERFACE_TYPE,
  GENERATE,
  HELP,
  COMPONENT,
  ROUTER,
  HOC
};
