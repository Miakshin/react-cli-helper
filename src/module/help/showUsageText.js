const usageText = require('./constants').usageText;

const showUsageText = () => console.log(usageText);

module.exports = {
  showUsageText
};
