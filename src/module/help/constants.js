const usageText = `
    rh helps uou to generate boilerplate code for components, hocks and routers
    
    usage:
        rh generate <command> <path-to-file>

    commands can be:

    component:      used to generate new folder with .conponent.tsx, .type.ts, .styles.css files
    router:         used generate new .routers.tsx file
    hoc:            used to print the usage guide
    
        rh help     show help text
`;

module.exports = {
  usageText
};
