const kebabCase = require("lodash/kebabCase");

const hocTemplate = hocName => `import * as React from 'react';

const ${hocName} = (WrappedComponent: React.ComponentType<any>) => (
  props: any
) => {
  // your logic here
  const updatedProps = props;
  return <WrappedComponent {...updatedProps} />;
};

export default ${hocName};
`;

const routerTemplate = routerName => `import React from 'react';
import { Redirect, Route, Switch } from 'react-router';

const ${routerName} = () => (
  <Switch>
    <Route path={/* add path here */} component={/* component here */} />
    <Redirect to={/* add redirect path here */} />
  </Switch>
);

export default ${routerName};
`;

const componentFileTemplate = componentName => {
  const fileName = kebabCase(componentName);
  const className = fileName;
  const interfaceName = `I${componentName}Props`;
  return `import React from 'react';

import { ${interfaceName} } from './${fileName}.type';
import './${fileName}.style.css';

const ${componentName}: React.FunctionComponent<${interfaceName}> = props => (
  <div className="${className}">Hello ${componentName}</div>
);

export default ${componentName};
`;
};

const componentTypesTemplate = componentName => {
  const interfaceName = `I${componentName}Props`;
  return `export interface ${interfaceName} {}
`;
};

const componentStylesTemplate = componentName => {
  const className = kebabCase(componentName);
  return `.${className} {}
`;
};

module.exports = {
  hocTemplate,
  routerTemplate,
  componentFileTemplate,
  componentTypesTemplate,
  componentStylesTemplate
};
