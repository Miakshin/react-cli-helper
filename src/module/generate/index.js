const path = require("path");

const includes = require("lodash/includes");
const upperFirst = require("lodash/upperFirst");
const camelCase = require("lodash/camelCase");
const kebabCase = require("lodash/kebabCase");
const showUsageText = require("../help/showUsageText").showUsageText;
const createDir = require("../../helpers/create-dir");
const createFile = require("../../helpers/create-file");
const getFileNameFromPath = require("../../helpers/get-file-name-from-path");
const COMPONENT = require("../../index.constants").COMPONENT;
const ROUTER = require("../../index.constants").ROUTER;
const HOC = require("../../index.constants").HOC;
const COMPONENT_TYPE = require("../../index.constants").COMPONENT_TYPE;
const ROUTER_TYPE = require("../../index.constants").ROUTER_TYPE;
const HOC_TYPE = require("../../index.constants").HOC_TYPE;
const STYLE_TYPE = require("../../index.constants").STYLE_TYPE;
const INTERFACE_TYPE = require("../../index.constants").INTERFACE_TYPE;
const subCommandsMaps = require("../../index.constants").subCommandsMaps;
const filesExtensionsMap = require("../../index.constants").filesExtensionsMap;
const routerTemplate = require("./templates").routerTemplate;
const hocTemplate = require("./templates").hocTemplate;
const componentFileTemplate = require("./templates").componentFileTemplate;
const componentTypesTemplate = require("./templates").componentTypesTemplate;
const componentStylesTemplate = require("./templates").componentStylesTemplate;

const args = process.argv;
const subCommand = args[3];
const pathToFile = args[4];

const getFullFileNameByType = (fileName, fileFolder, fileType) => {
  const availableTypes = Object.keys(filesExtensionsMap);
  if (!includes(availableTypes, fileType)) {
    throw "Invalid file type";
  }
  const fileNameWithExtension = `${fileName}${filesExtensionsMap[fileType]}`;
  return path.join(fileFolder, fileNameWithExtension);
};

const getFileDataByType = (type, entityName) => {
  switch (type) {
    case ROUTER_TYPE:
      return routerTemplate(entityName);
    case HOC_TYPE:
      return hocTemplate(entityName);
    case COMPONENT_TYPE:
      return componentFileTemplate(entityName);
    case STYLE_TYPE:
      return componentTypesTemplate(entityName);
    case INTERFACE_TYPE:
      return componentStylesTemplate(entityName);
    default:
      throw "Invalid data type";
  }
};

const generate = () => {
  if (!includes(subCommandsMaps.generate, subCommand) || !pathToFile) {
    showUsageText();
    return;
  }

  const name = getFileNameFromPath(pathToFile);
  const entityName = upperFirst(camelCase(name));
  const fileName = kebabCase(name);
  const folderPath = path.join(process.cwd(), path.dirname(pathToFile));

  const getFullFilePathByType = type =>
    getFullFileNameByType(fileName, folderPath, type);
  const getFileData = (type) => getFileDataByType(type, entityName);

  if (subCommand === COMPONENT) {
    createDir(folderPath)
      .then(() =>
        createFile(
          getFullFilePathByType(COMPONENT_TYPE),
          getFileData(COMPONENT_TYPE)
        )
      )
      .then(() =>
        createFile(getFullFilePathByType(STYLE_TYPE), getFileData(STYLE_TYPE))
      )
      .then(() =>
        createFile(
          getFullFilePathByType(INTERFACE_TYPE),
          getFileData(INTERFACE_TYPE)
        )
      );
  }
  if (subCommand === ROUTER) {
    createDir(folderPath).then(() =>
      createFile(getFullFilePathByType(ROUTER_TYPE), getFileData(ROUTER_TYPE))
    );
  }
  if (subCommand === HOC) {
    createDir(folderPath).then(() =>
      createFile(getFullFilePathByType(HOC_TYPE), getFileData(HOC_TYPE))
    );
  }
};

module.exports = generate;
