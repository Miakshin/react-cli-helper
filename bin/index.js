#!/usr/bin/env node

const commands = require("../src/index.constants").commands;
const HELP = require("../src/index.constants").HELP;
const GENERATE = require("../src/index.constants").GENERATE;
const logger = require("../src/helpers/logger").logger;
const ERROR = require("../src/helpers/logger").ERROR;
const showUsageText = require("../src/module/help/showUsageText.js").showUsageText;
const generate = require("../src/module/generate/index");
const includes = require("lodash/includes");

const args = process.argv;
const mainCommand = args[2];

if (!includes(commands, mainCommand) || mainCommand === HELP) {
    logger(ERROR, `Unrecognized command ${mainCommand}`);
    showUsageText();
}

if (mainCommand === HELP) {
    showUsageText();
}

if (mainCommand === GENERATE) {
  generate();
}
